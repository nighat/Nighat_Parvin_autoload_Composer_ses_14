<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 1/26/2017
 * Time: 11:27 PM
 */

namespace App;


class Person
{
    private $name;
    private $dateOfBirth;

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;
    }

    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }
}



