<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 1/26/2017
 * Time: 11:30 PM
 */

namespace Tap;


use App\Person;

class Student extends Person
{
     private $studentID;

    public function setStudentID($studentID)
    {
        $this->studentID = $studentID;
    }

    public function getStudentID()
    {
        return $this->studentID;
    }
}

