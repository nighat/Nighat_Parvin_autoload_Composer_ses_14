<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit5eb5910f47cd111ead7b001980fb34b2
{
    public static $prefixLengthsPsr4 = array (
        'T' => 
        array (
            'Tap\\' => 4,
        ),
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Tap\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src/BITM/SEIP_155644',
        ),
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src/BITM/SEIP_155644',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit5eb5910f47cd111ead7b001980fb34b2::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit5eb5910f47cd111ead7b001980fb34b2::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
